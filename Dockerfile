FROM alpine
ARG VERSION
RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories
RUN apk add murmur=${VERSION}-r0


ENTRYPOINT ["/usr/bin/murmurd"]

